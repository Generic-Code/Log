<#
.Synopsis
    A tool to log your script to a file when you call it.
.DESCRIPTION
    A tool to log your script to a file with if you want to. It will also allow you to show on-screen logging if needed.
    It is possible to set the below variables as you need, prior to the log function.

    $MainlogFile - This will be the log file name (the file extension must be .log).
    $MainlogPath - This will be the folder in which the log file will be created.
    $LogDebuggingMode - This will allow you to see the log in action (when setting it to $true), in addition to writing it to the log file.

    For example:
    $MainlogFile = "LogFileName.log"
    $MainlogPath = "C:\MyDirectory\"
    $LogDebuggingMode = $True

.EXAMPLE
    Log "This line is logged"
.EXAMPLE
    Log -$logThis "This line is logged" -MainlogFile History.log -MainlogPath C:\Folder\ -LogDebuggingMode -$true
.INPUTS
    Inputs to this cmdlet (if any)
.OUTPUTS
    Output from this cmdlet (if any)
.NOTES
    Copyright © 2017 admin@plonter.tk http://www.plonter.tk . All rights reserved.
.COMPONENT
    The component this cmdlet belongs to
.ROLE
    The role this cmdlet belongs to
.FUNCTIONALITY
    The functionality that best describes this cmdlet
#>
Function Write-Log {
    [CmdletBinding(SupportsShouldProcess=$true, ConfirmImpact='Low')]
    param (
        [Parameter(Mandatory=$True, valuefrompipeline=$true, ValueFromPipelineByPropertyName=$true, HelpMessage = "The actual text/object to be logged in the log file.")][Alias("TextToLog")]$logThis,
        [Parameter(Mandatory=$False, HelpMessage = "Set the log file name.")][ValidateScript({($_).EndsWith(".log")})][string]$MainlogFile,
        [Parameter(Mandatory=$False, HelpMessage = "Set the folder in which you want to save the log file.")][ValidateScript({($_).Contains(":\")})][string]$MainlogPath,
        [Parameter(Mandatory=$False, HelpMessage = 'Turn on the on-screen presentation of the log actions by setting to $true')][Boolean]$LogDebuggingMode
    )

    Begin {
    # The 'Begin' section will run once (in each time this function will be called)
        if (!$MainlogFile){$MainlogFile = (((Get-date).Year).ToString("0000") +"-"+ ((get-date).Month).ToString("00") +"-"+ ((get-date).Day).ToString("00")) + "-" + "NoLogFileName.log"}
        if (!$MainlogPath){$MainlogPath = ($env:TEMP)+"\"}
        if (!($MainlogPath).EndsWith("\")) {$MainlogPath = $MainlogPath+"\"}
        if (((test-path $MainlogPath$MainlogFile) -eq $false) -and ($MainlogFile -eq (((Get-date).Year).ToString("0000") +"-"+ ((get-date).Month).ToString("00") +"-"+ ((get-date).Day).ToString("00")) + "-" + "NoLogFileName.log")){Write-Warning "No LogFile name specified. using $MainlogFile as the log file name."}
        #################### Verify log path exist and craete it if it doesn't #######################
        
        if ((test-path -path $MainlogPath) -eq $false){
            if ($psCmdlet.shouldProcess("$MainlogPath","Create the folder in which the log file will be saved.")){
                write-host "Creating log folder" -ForegroundColor Yellow
                New-item $MainlogPath -type directory -Force |out-null
            }
        }
        ######################################## RighTime ############################################
        Function RighTime {
            $zone = ([TimeZoneInfo]::Local).DisplayName
            return ((get-date).Year).ToString("0000") +"/"+ ((get-date).Month).ToString("00")+"/"+ ((get-date).Day).ToString("00") +" $zone "+ ((get-date).hour).ToString("00") +":"+ ((get-date).Minute).ToString("00") +":"+ ((get-date).Second).ToString("00")
        }
        ######################################### LOGGER #############################################
        function Logger ($logThis) {
            $now = RighTime
            $message = "$now :`t $logThis"
            #--- For extra debugging ---
            if ($LogDebuggingMode -eq $True){
                If ($psCmdlet.shouldProcess("$LogDebuggingMode", "Write the live log content on the screen if LogDebugging mode is used")){
                    Write-host "$message" -ForegroundColor Yellow -BackgroundColor Black
                }
            }
            #---------------------------
            try {
                add-content -Path "$MainlogPath$MainlogFile" -Value "$message" -ErrorAction Stop -ErrorVariable ErrorVar
            }
            Catch {
                Write-host "Failed to add contect to the log file because:`n$ErrorVar" -ForegroundColor Red
            }
        }
        ##############################################################################################
    }
    Process {
    # The 'Process' section will for each object that will come through the pipeline
        Logger $logThis
    }
    End {
    # The 'End' section will run once (in the end of the script)
    }
}
